#!/usr/bin/env python3
import cgi
import datetime
import html
import json

form    = cgi.FieldStorage()
action  = form.getfirst("action", "")
action  = html.escape(action)

if action == 'hellow':
    sRes = 'Hi'
elif action == 'time':
    now = datetime.datetime.now()
    sRes = now.strftime("%d.%m.%Y %H:%M")    
else:
    sRes = 'Undefined'

jresult =  {'success':'true',
            'answer' :f'{sRes}'
           }

print("Content-type:application/json\r\n\r\n")
print(json.dumps(jresult))